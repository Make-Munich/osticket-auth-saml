<?php

require_once INCLUDE_DIR . '/class.plugin.php';
require_once INCLUDE_DIR . '/class.forms.php';

class SamlAuthConfig extends PluginConfig {

    function getOptions() {
        return array(
            'idp_entity_id' => new TextboxField(
                array(
                    'id' => 'idp_entity_id',
                    'label' => 'Identity Provider Entity ID',
                    'configuration' => array(
                        'size' => 59,
                        'length' => 255
                    )
                )
            ),
            'idp_cert_fingerprint' => new TextboxField(
                array(
                    'id' => 'idp_cert_fingerprint',
                    'label' => 'Identity Provider Certificate Fingerprint',
                    'configuration' => array(
                        'size' => 59,
                        'length' => 59
                    )
                )
            ),
            'idp_sso_target_url' => new TextboxField(
                array(
                    'id' => 'idp_sso_target_url',
                    'label' => 'Identity Provider SSO URL',
                    'configuration' => array(
                        'size' => 59,
                        'length' => 255
                    )
                )
            ),
            'entity_id' => new TextboxField(
                array(
                    'id' => 'entity_id',
                    'label' => 'Service Provider Entity ID',
                    'configuration' => array(
                        'size' => 59,
                        'length' => 255
                    )
                )
            ),
            'assertion_consumer_service_url' => new TextboxField(
                array(
                    'id' => 'assertion_consumer_service_url',
                    'label' => 'Assertion Consumer Service URL',
                    'configuration' => array(
                        'size' => 59,
                        'length' => 255
                    )
                )
            ),
        );
    }
}