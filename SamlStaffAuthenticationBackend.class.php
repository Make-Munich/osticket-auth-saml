<?php

require_once 'lib/xmlseclibs/xmlseclibs.php';
require_once 'lib/Saml2/Settings.php';
require_once 'lib/Saml2/AuthnRequest.php';
require_once 'lib/Saml2/Error.php';
require_once 'lib/Saml2/Utils.php';
require_once 'lib/Saml2/Response.php';
require_once 'lib/Saml2/Constants.php';


class SamlStaffAuthenticationBackend extends ExternalStaffAuthenticationBackend {

    static $id = "saml";
    static $name = "SAML";
    static $service_name = "SAML";

    private $_config;

    function __construct($config) {
        $this->_config = $config;

        $self = $this;

        Signal::connect('api', function ($dispatcher) use ($self) {
            $dispatcher->append(
                url('^/auth/saml$', function () use ($self) {
                    if (isset($_POST['SAMLResponse'])) {
                        $response = $self->getSamlResponse($_POST['SAMLResponse']);

                        if ($response->isValid()) {
                            $_SESSION['saml']['nameId'] = $response->getNameId();

                            Http::redirect( ROOT_PATH . 'scp' );
                        }
                    }
                })
            );
        });
    }

    private function getSamlConfiguration() {
        $settings = array (
            'idp' => array (
                'entityId' => $this->_config->get('idp_entity_id'),
                'singleSignOnService' => array (
                    'url' => $this->_config->get('idp_sso_target_url'),
                    'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                ),
                'certFingerprint' => $this->_config->get('idp_cert_fingerprint')
            ),

            'sp' => array (
                'entityId' => $this->_config->get('entity_id'),
                'assertionConsumerService' => array (
                    'url' => $this->_config->get('assertion_consumer_service_url'),
                    'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                ),
                'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient'
            ),
        );

        return new OneLogin_Saml2_Settings($settings);
    }

    public function createSignInURL() {
        $settings = $this->getSamlConfiguration();
        $authRequest = new OneLogin_Saml2_AuthnRequest($settings);
        $samlRequest = $authRequest->getRequest();

        $parameters = array('SAMLRequest' => $samlRequest);

        $idpData = $settings->getIdPData();
        $ssoUrl = $idpData['singleSignOnService']['url'];
        $url = OneLogin_Saml2_Utils::redirect($ssoUrl, $parameters, true);

        return $url;
    }

    public function getSamlResponse($saml_response) {
        return new OneLogin_Saml2_Response($this->getSamlConfiguration(), $saml_response);
    }

    function signOn() {
        if (isset($_SESSION['saml']['nameId'])) {
            $staff_id = StaffSession::getIdByEmail($_SESSION['saml']['nameId']);

            if ($staff_id) {
                if (method_exists(StaffSession, 'lookup')) {
                    // Assholes
                    $staff_session = StaffSession::lookup($staff_id);
                } else {
                    $staff_session = new StaffSession($staff_id);
                }

                return $staff_session;
            } else {
                $_SESSION['_staff']['auth']['msg'] = 'Have your administrator create a local account';
            }
        }
    }

    function triggerAuth() {
        # TODO : Set location header
        parent::triggerAuth();

        Http::redirect($this->createSignInURL());
    }
}