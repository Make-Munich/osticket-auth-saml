<?php

// set_include_path( get_include_path() . PATH_SEPARATOR . dirname(__file__) . '/include' );

return array(
    'id' => 'auth:saml',
    'version' => '0.1',
    'name' => 'SAML Authentication and Lookup',
    'author' => 'Pontus Karlsson <pontus@thefarm.se>',
    'description' => 'Provides an authentication backend for SAML identity providers.',
    'plugin' => 'authentication.php:SamlAuthPlugin'
);